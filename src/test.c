#include <sys/unistd.h>
#include "test.h"


struct initial_struct{
    void* heap;
    struct block_header* block_header;
};
static const size_t INITIAL_PIECE_SIZE = 666;
static const size_t INITIAL_HEAP_SIZE = 10000;

void debug(const char* fmt, ... );

static struct block_header* block_get_header(void* contents){
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

static void print_newline(){
    printf("\n<---------------------->\n");
}

static struct initial_struct* init(struct initial_struct* initial_struct){
    printf("Initializing heap.");
    print_newline();
    initial_struct->heap = heap_init(INITIAL_HEAP_SIZE);
    if (initial_struct->heap == NULL){
        debug("Heap is not initialized.");
        print_newline();
    }
    return initial_struct;
}

static void test1(){
    struct initial_struct initial_struct = {.heap = NULL, .block_header=NULL };
    initial_struct.heap = init(&initial_struct);
    initial_struct.block_header = (struct block_header*) (initial_struct.heap);
    printf("Test 1. Let's start...");
    print_newline();

    void* piece = _malloc(INITIAL_PIECE_SIZE);
    struct block_header* piece_block = block_get_header(piece);
    if (piece == NULL) err("Test 1 failed. Memory wasn't allocated.");
    if (piece_block->is_free) err("Test 1 failed. Memory isn't free.");
    if (piece_block->capacity.bytes != INITIAL_PIECE_SIZE) err("Test 1 failed. Capacity not equals matched value.");
    _free(piece);
    printf("Test 1 passed. 4 more tests left.");
    print_newline();
}

static void test2(){
    struct initial_struct initial_struct = {.heap = NULL, .block_header=NULL };
    initial_struct.heap = init(&initial_struct);
    initial_struct.block_header = (struct block_header*) (initial_struct.heap);
    debug("Test 2. Let's start...");
    print_newline();

    void* piece1 = _malloc(INITIAL_PIECE_SIZE);
    void* piece2 = _malloc(INITIAL_PIECE_SIZE);

    if (piece1 == NULL || piece2 == NULL) err("Test 2 failed. Memory wasn't allocated.");
    _free(piece1);
    struct block_header* piece1_block = block_get_header(piece1);
    struct block_header* piece2_block = block_get_header(piece2);
    if (!piece1_block->is_free) err("Test 2 failed. Freed block is not empty.");
    if (piece2_block->is_free) err("Test 2 failed. Not freed block is empty.");
    debug("Test 2 passed. 3 more tests left.");
    _free(piece2);
    print_newline();
}

void test3(){
    struct initial_struct initial_struct = {.heap = NULL, .block_header=NULL };
    initial_struct.heap = init(&initial_struct);
    initial_struct.block_header = (struct block_header*) (initial_struct.heap);
    debug("Test 3. Let's start...");
    print_newline();

    void* piece1 = _malloc(INITIAL_PIECE_SIZE);
    void* piece2 = _malloc(INITIAL_PIECE_SIZE);
    void* piece3 = _malloc(INITIAL_PIECE_SIZE);

    if (piece1 == NULL || piece2 == NULL || piece3 == NULL) err("Test 3 failed. Memory wasn't allocated.");
    _free(piece1);
    _free(piece2);
    struct block_header* piece1_block = block_get_header(piece1);
    struct block_header* piece2_block = block_get_header(piece2);
    struct block_header* piece3_block = block_get_header(piece3);
    if (piece1_block->is_free == false || piece2_block->is_free == false) err("Test 3 failed. Freed block is not empty.");
    if (piece3_block->is_free != false) err("Test 3 failed. Not freed block is empty.");
    debug("Test 3 passed. 2 more tests left.");
    _free(piece3);
    print_newline();
}

static void test4(){
    struct initial_struct initial_struct = {.heap = NULL, .block_header=NULL };
    initial_struct.heap = init(&initial_struct);
    initial_struct.block_header = (struct block_header*) (initial_struct.heap);
    debug("Test 4. Let's start...");
    print_newline();

    void* piece1 = _malloc(INITIAL_HEAP_SIZE*2);
    void* piece2 = _malloc(INITIAL_HEAP_SIZE*2);
    if (piece1 == NULL || piece2 == NULL) err("Test 4 failed. Memory wasn't allocated.");

    struct block_header* piece1_block = block_get_header(piece1);
    struct block_header* piece2_block = block_get_header(piece2);
    if (piece1_block->next != piece2_block) err("Test 4 failed. Blocks isn't linked.");
    if ((uint8_t*) piece1_block->contents + piece1_block->capacity.bytes != (uint8_t*) piece2_block) err("Test 4 failed. Blocks are not follows each other.");
    debug("Test 4 passed. 1 more tests left..");
    _free(piece1);
    _free(piece2);
    print_newline();
}

static void test5(){
    struct initial_struct initial_struct = {.heap = NULL, .block_header=NULL };
    initial_struct.heap = init(&initial_struct);
    initial_struct.block_header = (struct block_header*) (initial_struct.heap);
    debug("Test 5. Let's start...");
    print_newline();

    void* part2_address = (uint8_t*) initial_struct.block_header + initial_struct.block_header->capacity.bytes + 66;
    void* new_heap = mmap((uint8_t*) (getpagesize() * ((size_t)part2_address / getpagesize() + (((size_t)part2_address % getpagesize()) > 0))), 666, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_PRIVATE, 0, 0);
    void* piece1 = _malloc(INITIAL_HEAP_SIZE*5);
    if (piece1 == part2_address) err(" Test 5 failed. Block was created next to started allocated heap.");
    _free(piece1);
    debug("Test 5 passed. Grats, vse testi passed.");
    munmap(new_heap, (getpagesize() * ((size_t)part2_address / getpagesize() + (((size_t)part2_address % getpagesize()) > 0))));
}
void test_start(){
    test1();
    test2();
    test3();
    test4();
    test5();
}
